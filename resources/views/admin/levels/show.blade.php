@extends('admin.layouts.master')

@section('content')
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix">
            <div class="pull-left">
                <h4 class="text-blue">Show Class</h4>
                <p class="mb-30 font-14">Classes</p>
            </div>
            <div class="pull-right">
                <a href="{{url('admin/levels')}}" class="btn btn-primary btn-sm "  role="button"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-12 col-md-2 col-form-label font-weight-bold">Class Name</label>
                <input type="text" readonly class="form-control ml-2" value="{{ $level->name }}">
            </div>
        </div>
    </div>
@endsection
