@extends('admin.layouts.master')

@section('content')
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Classes</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Classes</li>
                        </ol>
                    </nav>

                </div>
            </div>
        </div>
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
                <div class="clearfix mb-20">
                    <div class="pull-left">
                        <h4 class="text-blue">Classes</h4>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('/home')}}" class="btn btn-primary btn-sm "  role="button"><i class="fa fa-arrow-left"></i>Back</a>
                        <a href="{{url('admin/levels/create')}}" class="btn btn-info btn-sm "  role="button"> Create New Class</a>
                    </div>
                </div>
                @if(session('message'))
                <div  class="alert {{ Session('alert-class', 'alert-success','alert-block') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('message') }}</strong>
                </div>
                @endif
                    <table class="stripe hover multiple-select-row data-table-export nowrap">
                        <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Class Name</th>
                            <th>Create Date</th>
                            <th>Create By</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 0; @endphp
                        @foreach($levels as $level)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $level->name }}</td>
                                <td>{{ date('d-M-Y',strtotime($level->created_at)) }}</td>
                                <td>{{ $level->user->name }}</td>
                                <td>
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="{{ url('/admin/levels/'.$level->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a> </li>
                                        <li class="list-inline-item"><a href="{{ url('/admin/levels/'.$level->id.'/edit') }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a> </li>
                                        <li class="list-inline-item">
                                            {!! Form::open(['url' => ['admin/levels', $level->id], 'method' => 'delete']) !!}
                                            {!! Form::button("<i class='fa fa-trash'></i>",['type' => 'submit','onClick' =>"return confirm('Are You sure wante to delete $level->name ?')",'class' => 'btn btn-sm btn-danger lkj']) !!}
                                            {!! Form::close() !!}
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
        </div>
    </div>
@endsection
@include('admin.include.js')
<script>
    $("document").ready(function(){
        setTimeout(function() {
            $('.alert').fadeOut('fast');
        }, 3000);
    });
</script>


