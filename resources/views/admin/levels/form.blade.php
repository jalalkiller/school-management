
    <div class="form-group row{{ $errors->has('name') ? 'has-error' : '' }}">
        {!! Form::label('name','Name',["class"=>"col-md-12 col-md-2 col-form-label font-weight-bold"]) !!}
        <div class="col-md-12 col-md-10">
            {!! Form::text('name',null,["class" => "form-control jalal",'required','autofocus']) !!}

            @if($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row pull-right">
        <div class="col-md-12">
            {!! Form::submit('Submit',["type"=>"submit","class"=>"btn btn-primary btn-sm"]) !!}
        </div>
    </div>





