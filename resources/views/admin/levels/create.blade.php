@extends('admin.layouts.master')

@section('content')
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix">
            <div class="pull-left">
                <h4 class="text-blue">Create Class</h4>
                <p class="mb-30 font-14">Classes</p>
            </div>
            <div class="pull-right">
                <a href="{{url('admin/levels')}}" class="btn btn-primary btn-sm "  role="button"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
        {!! Form::open(['url' => 'admin/levels']) !!}
        @include('admin.levels.form')
        {!! Form::close() !!}
    </div>
@endsection
