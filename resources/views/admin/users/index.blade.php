@extends('admin.layouts.master')

@section('content')
    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Users</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Users</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
            <div class="row">
                    <div class="clearfix mb-20">
                        <div class="pull-left">
                            <h4 class="text-blue">Users</h4>
                        </div>
                    </div>
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">#</th>
                            <td>{{ $users->name }}</td>
                            <td>{{ $users->email }}</td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="{{ url('/admin/users/'.$users->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a> </li>
                                    <li class="list-inline-item"><a href="{{ url('/admin/users/'.$users->id.'/edit') }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a> </li>
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@endsection

