@extends('admin.layouts.master')

@section('content')
            <div class="row">
                <div class="col-lg-12 col-md-12 m-0 mb-2">
                    <div class="card">
                        <div class="page-header ml-4 mt-3">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="title">
                                        <h4>Users</h4>
                                    </div>
                                    <nav aria-label="breadcrumb" role="navigation">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <hr class="mt-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="col-md-3 font-weight-bold">Name</label>
                                    <input type="text" readonly class="form-control" value="{{ $user->name }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-md-3 font-weight-bold">Email</label>
                                    <input type="text" readonly class="form-control" value="{{ $user->email }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
