@extends('admin.layouts.master')

@section('content')
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="clearfix">
            <div class="pull-left">
                <h4 class="text-blue">Edit user</h4>
                <p class="mb-30 font-14">Users</p>
            </div>
            <div class="pull-right">
                <a href="{{url('admin/users')}}" class="btn btn-primary btn-sm "  role="button"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
        {!! Form::open(['url' =>['admin/users',$user->id], 'method' =>'put']) !!}

        <div class="form-group row{{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('name',"Name", ["class" => "col-md-12 col-md-2 col-form-label"]) !!}
            <div class="col-md-12 col-md-10">
                {!! Form::text('name',$user->name, ["class" => "form-control",'required','autofocus']) !!}

                @if($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row{{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('email',"Email", ["class" => "col-md-12 col-md-2 col-form-label"]) !!}
            <div class="col-md-12 col-md-10">
                {!! Form::text('email',$user->email, ["class" => "form-control",'required','autofocus']) !!}

                @if($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-12 col-md-2 col-form-label">{{ __('Password') }}</label>

            <div class="col-md-12 col-md-10">
                <input id="password" type="password" value="{{ $user->password }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-12 col-md-2 col-form-label">{{ __('Confirm Password') }}</label>

            <div class="col-md-12">
                <input id="password-confirm" type="password" value="{{ $user->password }}" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
        </div>

        <div class="form-group pull-right">
            <div class="col-md-12">
                {!! Form::submit('Update',["type" => "submit" ,"class" => "btn btn-primary"]) !!}
            </div>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
